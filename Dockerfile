FROM ubuntu:latest
ENV TZ=Europe/Moscow 
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN apt-get update && \
    apt-get install -y nginx nodejs npm openjdk-11-jdk postgresql postgresql-contrib git curl wget && \
    npm install n -g && \
    n latest
RUN git clone https://github.com/SiLa4-Cyber/polkadot_metrics.git /app     
RUN service postgresql start && \
    su postgres -c 'createdb mydatabase'
COPY . /app
WORKDIR /app
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]